package hk.quantr.ttl;

import hk.quantr.logic.LogicGateDrawer;
import hk.quantr.logic.data.gate.Vertex;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TTL7411 extends Vertex {
	
	public TTL7411(String name) {
		super(name, 4, 10, 5, 4);
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		
		Graphics2D g2 = LogicGateDrawer.init(g);
		BasicStroke stroke = new BasicStroke(this.gridSize / 5, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		g2.setStroke(stroke);
		g2.setColor(Color.black);
		g2.drawRect(this.x * gridSize, this.y * gridSize, width * gridSize, height * gridSize);
		g.setFont(g.getFont().deriveFont(24));
		g.drawString("7411", (this.x + 1) * gridSize, (this.y + 1) * gridSize);
	}
	
	@Override
	public void eval() {
	}
	
}
