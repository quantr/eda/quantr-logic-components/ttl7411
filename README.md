# Introduction

Component to load into Quantr Logic https://gitlab.com/quantr/eda/quantr-logic

# Explain

1. mvn package will get the jar

2. the jar will contain component.xml and icon.png

```
<component>
	<icon>src/main/resources/icon.png</icon>
	<description>Three 3-Input AND Gates</description>
	<developers>
		<developer>
			<name>Peter</name>
			<email>peter@quantr.hk</email>
			<organization>Quantr Foundation</organization>
		</developer>
	</developers>
</component>
```

3. upload the jar to https://www.quantr.foundation/logic-component-library/

# How Quantr Logic load the component

